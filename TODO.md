# To Do List/Support Matrix:
- Intergrate: Practical-Cryptography-for-Developers-Book into Documentation, and help with structuring the API better.
- Add further Examples
- Intergrate OQS pq algos

# Learning Encryption

## 1. Symmetric Ciphers
Symmetric cryptography (also known as secret key cryptography): involves using the same key for encryption and decryption.

### A. Stream Ciphers
Stream ciphers: encrypt a continuous stream of data one bit or byte at a time.

AES-GCM
ChaCha20-Poly1305
XSalsa20
Sosemanuk
Grain v1

### B. Block Ciphers
Block ciphers: encrypts fixed-size blocks of data, for example, encrypting 128-bit blocks of data using AES.

AES (Advanced Encryption Standard)
Twofish
Serpent
Blowfish
CAST-128 (also known as CAST5)

## 2. Asymmetric Ciphers
Asymmetric cryptography (also known as public key cryptography): involves using a pair of keys, one for encryption and one for decryption.

### A. Stream Ciphers
Stream ciphers: encrypt a continuous stream of data one bit or byte at a time.

RSA-OAEP
Elliptic Curve Diffie-Hellman (ECDH)
Elliptic Curve Digital Signature Algorithm (ECDSA)
Hybrid Encryption (combination of asymmetric and symmetric encryption)
DHIES (Diffie-Hellman Integrated Encryption Scheme)

### B. Block Ciphers
Block ciphers: encrypts fixed-size blocks of data, for example, encrypting 128-bit blocks of data using AES.

RSA (Rivest-Shamir-Adleman)
Elliptic Curve Cryptography (ECC)
ElGamal
DH (Diffie-Hellman)
DSA (Digital Signature Algorithm)


## 3. Hashing
Hashing: involves converting an input (or "message") into a fixed-size output (hash) which is typically used for integrity and authentication.

SHA-3 (Secure Hash Algorithm 3)
Blake2
Whirlpool
SHA-256 (Secure Hash Algorithm 256-bit)
SHA-512 (Secure Hash Algorithm 512-bit)


## 4. ECC
Elliptic Curve Cryptography (ECC): a form of public-key cryptography based on the mathematics of elliptic curves.

## 5. Stenography
Steganography: the practice of hiding a secret message within an ordinary message and the extraction of it at its destination.




## 8. Substitution ciphers
Substitution ciphers: a type of cryptography in which each letter of the plaintext is replaced with another letter or number.

## 9. Transposition ciphers
Transposition ciphers: a type of cryptography in which the positions of the letters are rearranged to create the ciphertext.

## 10. Classical ciphers
Classical ciphers: a type of cryptography that was used before the invention of modern cryptography and includes substitution and transposition ciphers.

## 11. Codebook ciphers
Codebook ciphers: a type of cryptography in which each word or phrase in the plaintext is replaced with a code word or symbol.

## 12. Quantum cryptography
Quantum cryptography: a type of cryptography that uses the principles of quantum mechanics to secure communication.

## 13. Homophonic ciphers
Homophonic ciphers: a type of cryptography in which each letter of the plaintext is replaced with multiple symbols to reduce the frequency of letters and make it more difficult to analyze the ciphertext.

## 14. Book ciphers
Book ciphers: a type of cryptography in which the plaintext is hidden in a book or other text.

## 15. Multimedia steganography
Multimedia steganography: a type of cryptography that involves hiding a secret message within an image, audio, or video file.
