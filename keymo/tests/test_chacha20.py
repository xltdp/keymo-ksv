import unittest
from keymo.api.symmetric.chacha20poly1305 import ChaCha20Poly1305


class TestChaCha20Poly1305(unittest.TestCase):
    def setUp(self):
        password = b"password"
        salt = b"salt"
        self.cipher = ChaCha20Poly1305(password, salt)

    def test_encrypt(self):
        data = b"data"
        encrypted_data, tag = self.cipher.encrypt(data)
        self.assertNotEqual(data, encrypted_data)
        self.assertEqual(len(tag), 16)

    def test_decrypt(self):
        data = b"data"
        encrypted_data, tag = self.cipher.encrypt(data)
        decrypted_data = self.cipher.decrypt(encrypted_data, tag)
        self.assertEqual(data, decrypted_data)

    def test_verify(self):
        # Encrypt some data and get the tag
        data = b"secret message"
        encrypted_data = self.cipher.encrypt(data)
        tag = self.cipher.get_tag()

        encrypted_data, tag = self.cipher.encrypt(data)

        # Check that verify returns True for the original data and tag
        self.assertTrue(self.cipher.verify(encrypted_data, tag))

        # Check that verify returns False for a modified data and original tag
        modified_data = encrypted_data[:-1] + b"0"
        self.assertFalse(self.cipher.verify(modified_data, tag))

        # Check that verify returns False for original data and a modified tag
        modified_tag = tag[:-1] + b"0"
        self.assertFalse(self.cipher.verify(encrypted_data, modified_tag))
