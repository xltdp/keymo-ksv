import unittest
from cryptography.hazmat.primitives.kdf.scrypt import Scrypt
from cryptography.hazmat.backends import default_backend
from keymo.api.symmetric.scrypt import SCRYPT


class TestSCRYPT(unittest.TestCase):
    def setUp(self):
        self.password = b"password"
        self.salt = b"salt"
        self.N = 16384
        self.r = 8
        self.p = 1
        self.backend = default_backend()
        self.scrypt = SCRYPT(
            self.password, self.salt, self.N, self.r, self.p
        )  # noqa: E501
        self.kdf = Scrypt(
            salt=self.salt,
            length=64,
            n=self.N,
            r=self.r,
            p=self.p,
            backend=self.backend,
        )
        self.message = "message"
        self.key = self.kdf.derive(self.password)
        self.encrypted_message = self.message.encode() + self.key
        self.data = b"data"
        self.wrapped_key_and_data = self.key + self.data

    def test_init(self):
        self.assertEqual(self.scrypt.password, self.password)
        self.assertEqual(self.scrypt.salt, self.salt)
        self.assertEqual(self.scrypt.N, self.N)
        self.assertEqual(self.scrypt.r, self.r)
        self.assertEqual(self.scrypt.p, self.p)
        self.assertEqual(self.scrypt.backend, self.backend)

    def test_create_kdf(self):
        kdf = self.scrypt._create_kdf()
        self.assertIsInstance(kdf, Scrypt)

    def test_encrypt(self):
        encrypted_message = self.scrypt.encrypt(self.message)
        self.assertEqual(encrypted_message, self.encrypted_message)

    def test_decrypt(self):
        decrypted_message = self.scrypt.decrypt(self.encrypted_message)
        self.assertEqual(decrypted_message, self.message)

    def test_generate_key(self):
        key = self.scrypt.generate_key()
        self.assertEqual(key, self.key)

    def test_verify(self):
        result = self.scrypt.verify(self.encrypted_message)
        self.assertTrue(result)

    def test_wrap(self):
        wrapped_key_and_data = self.scrypt.wrap(self.data)
        self.assertEqual(wrapped_key_and_data, self.wrapped_key_and_data)

    def test_unwrap(self):
        data = self.scrypt.unwrap(self.wrapped_key_and_data)
        self.assertEqual(data, self.data)
