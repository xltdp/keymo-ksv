import unittest
from cryptography.hazmat.primitives.kdf.scrypt import Scrypt
from cryptography.hazmat.backends import default_backend
from keymo.api.symmetric.scrypt import SCRYPT


class TestSCRYPT(unittest.TestCase):
    def setUp(self):
        self.password = b"password"
        self.salt = b"salt"
        self.N = 16384
        self.r = 8
        self.p = 1
        self.backend = default_backend()
        self.scrypt = SCRYPT(self.password, self.salt, self.N, self.r, self.p)
        self.message = "Hello, World!"
        self.encrypted_message = self.scrypt.encrypt(self.message)

    def test_create_kdf(self):
        kdf = self.scrypt._create_kdf()
        self.assertIsInstance(kdf, Scrypt)

    def test_encrypt(self):
        self.assertIsInstance(self.encrypted_message, bytes)

    def test_decrypt(self):
        decrypted_message = self.scrypt.decrypt(self.encrypted_message)
        self.assertEqual(decrypted_message, self.message)

    def test_generate_key(self):
        key = self.scrypt.generate_key()
        self.assertIsInstance(key, bytes)

    def test_verify(self):
        self.assertTrue(self.scrypt.verify(self.encrypted_message))

    def test_wrap(self):
        data = b"data"
        wrapped_key_and_data = self.scrypt.wrap(data)
        self.assertIsInstance(wrapped_key_and_data, bytes)

    def test_unwrap(self):
        data = b"data"
        wrapped_key_and_data = self.scrypt.wrap(data)
        original_key_and_data = self.scrypt.unwrap(wrapped_key_and_data)
        self.assertEqual(original_key_and_data, data)
