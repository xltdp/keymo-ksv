import unittest
from keymo.api.symmetric.aes import AES


class TestAES(unittest.TestCase):
    def test_encrypt_decrypt_cycle(self):
        aes = AES()
        data = b"This is some test data"
        encrypted = aes.encrypt(data)
        decrypted = aes.decrypt(encrypted)
        self.assertEqual(data, decrypted)

    def test_encrypt_decrypt_with_custom_key(self):
        key = b"Sixteen byte key"
        aes = AES(key=key)
        data = b"This is some test data"
        encrypted = aes.encrypt(data)
        decrypted = aes.decrypt(encrypted)
        self.assertEqual(data, decrypted)

    def test_decrypt_with_invalid_ciphertext(self):
        aes = AES()
        encrypted = b"This is not valid ciphertext"
        with self.assertRaises(ValueError) as ex:
            aes.decrypt(encrypted)
        self.assertEqual(
            str(ex.exception), "Invalid ciphertext, unable to decrypt."
        )  # noqa: E501

    def test_decrypt_with_incorrect_key(self):
        aes = AES()
        key = b"This is an incorrect key"
        aes2 = AES(key=key)
        encrypted = aes.encrypt(b"This is some test data")
        with self.assertRaises(ValueError) as ex:
            aes2.decrypt(encrypted)
        self.assertEqual(
            str(ex.exception), "Invalid ciphertext, unable to decrypt."
        )  # noqa: E501
