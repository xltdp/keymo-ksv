# Collision-resistant hashing algorithms, also known as cryptographic hash
# functions, are a type of hash function that are designed to make it
# difficult to find two different messages that produce the same hash
# value, also known as a collision. A collision-resistant hash function is
# an essential component of many cryptographic systems, such as digital
# signatures, message authentication, and data integrity protection.

# A good collision-resistant hash function has the following properties:

# Pre-image resistance: Given a hash value, it is computationally infeasible
# to find a message that would produce that hash value.

# Second pre-image resistance: Given a message, it is computationally
# infeasible to find a second message that produces the same hash value as
# the original message.

# Collision resistance: It is computationally infeasible to find two
# different messages that produce the same hash value.

# A common use of collision-resistant hash functions is to produce a short,
# fixed-length representation of a longer message or data. For example, a
# digital signature can be computed over a hash value of a message, instead
# of the entire message. This is more efficient, and also ensures that the
# signature is resistant to tampering, since changing even a small part of
# the message would produce a different hash value.

# In summary, collision-resistant hashing algorithms are an important part
# of many cryptographic systems, and are designed to make it difficult to
# find collisions, or two different messages that produce the same hash
# value.

# Example #1
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes

backend = default_backend()
message = b"message"

hash = hashes.Hash(hashes.SHA256(), backend)
hash.update(message)
digest = hash.finalize()

print(digest.hex())

# In this example, we first create a hash object using the hashes.Hash
# constructor and specifying the hash function (SHA-256) and the backend.
# Then, we use the update method to add the message to the hash object, and
# the finalize method to compute the hash value. The resulting hash value is
# a byte string, which we can convert to a hexadecimal string for printing.

# In this way, you can use any of the supported hash functions in the
# cryptography module to compute hash values, depending on your security
# requirements.
