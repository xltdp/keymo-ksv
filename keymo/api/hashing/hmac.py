# Keyed Hashing Algorithms, also known as Keyed-Hash Message Authentication
# Codes (HMAC), are a type of hash function that use a secret key in
# addition to the message to produce a fixed-length hash value. The key is
# used to "seed" the hash function, and provides an extra layer of security
# to the resulting hash value.

# The basic idea behind HMACs is to use a secret key to ensure that the
# message can only be hashed by someone who knows the key. This makes it
# difficult for an attacker to modify the message without being detected,
# since the hash value will change if the message is changed.

# HMACs are widely used in secure communication protocols, such as SSL/TLS
# and SSH, to verify the integrity and authenticity of messages. They are
# also used in digital signatures, where the hash value is signed with a
# private key to provide non-repudiation.

# In summary, Keyed Hashing Algorithms provide an extra layer of security
# compared to normal hash functions, and are widely used for secure
# communication and digital signature applications.

# The cryptography module in Python does support Keyed Hashing Algorithms,
# specifically the HMAC (Keyed-Hash Message Authentication Code) algorithm.
# The HMAC class in the cryptography module implements the HMAC algorithm,
# and provides a simple and convenient way to use HMAC for message
# authentication.

# Here is an example of how to use the HMAC class in the cryptography module
# to compute an HMAC value for a message:

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, hmac

backend = default_backend()
key = b"key"
message = b"message"

# h = hmac.HMAC(key, hashes.SHA384(), backend)
h = hmac.HMAC(key, hashes.SHA256(), backend)
h.update(message)
mac = h.finalize()

print(mac.hex())

# In this example, the key and message are both byte strings, and the HMAC is
# computed using the SHA-256 hash algorithm. The resulting HMAC value is a
# bytes object, which can be encoded as a hexadecimal string for printing or
# storage.
