import os
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives.hmac import HMAC


class ChaCha20HMAC:
    def __init__(self, password, salt):
        self.backend = default_backend()
        self.salt = salt
        self.key = self._derive_key(password)
        self.nonce = os.urandom(16)

    def _derive_key(self, password):
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=self.salt,
            iterations=100000,
            backend=self.backend,
        )
        return kdf.derive(password)

    def encrypt(self, data):
        cipher = Cipher(
            algorithms.ChaCha20(self.key, self.nonce),
            modes.CTR(self.nonce),
            backend=self.backend,
        )
        encryptor = cipher.encryptor()
        encrypted_data = encryptor.update(data) + encryptor.finalize()

        h = HMAC(self.key, hashes.SHA256(), backend=self.backend)
        h.update(encrypted_data)
        mac = h.finalize()

        return encrypted_data, mac

    def decrypt(self, encrypted_data, mac):
        h = HMAC(self.key, hashes.SHA256(), backend=self.backend)
        h.update(encrypted_data)
        try:
            h.verify(mac)
        except Exception:
            raise ValueError(
                "Data has been tampered with or password is incorrect"
            )  # noqa: E501

        cipher = Cipher(
            algorithms.ChaCha20(self.key, self.nonce),
            modes.CTR(self.nonce),
            backend=self.backend,
        )
        decryptor = cipher.decryptor()
        return decryptor.update(encrypted_data) + decryptor.finalize()

    def verify(self, encrypted_data, mac):
        h = HMAC(self.key, hashes.SHA256(), backend=self.backend)
        h.update(encrypted_data)
        try:
            h.verify(mac)
        except Exception:
            return False
        return True
