import os
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import padding, keywrap
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes


class AES:
    def __init__(self, key=None, mode=modes.CBC):
        """
        Initializes the class with an optional key and mode. If no key is
        provided, a random one is generated. If no mode is provided, CBC is
        used by default.
        """
        if key:
            self.key = key
        else:
            self.key = os.urandom(32)

        if mode is modes.CBC:
            self.key = os.urandom(16)

        self.mode = mode

    def generate_key(self, key_size=256):
        """
        Generates a random key of the specified size.
        The key size must be 16, 24, or 32 bytes for AES-128, AES-192, or
        AES-256, respectively.
        """
        if key_size >= 512 and self.mode in (modes.GCM, modes.CCM):
            self.key = os.urandom(64)
        elif key_size >= 256 and self.mode in (modes.GCM, modes.CCM):
            self.key = os.urandom(32)
        else:
            self.key = os.urandom(16)

    def encrypt(self, data):
        """
        Encrypts the input data using the selected mode of operation and key.
        The data is padded before encryption.
        """
        data = self._pad(data)

        cipher = Cipher(
            algorithms.AES(self.key),
            self.mode(self.key),
            backend=default_backend(),  # noqa: E501
        )
        encryptor = cipher.encryptor()
        ciphertext = encryptor.update(data) + encryptor.finalize()

        return ciphertext

    def decrypt(self, ciphertext):
        """
        Decrypts the input ciphertext using the selected mode of operation
        and key. The data is unpadded after decryption.
        """
        try:
            cipher = Cipher(
                algorithms.AES(self.key),
                self.mode(self.key),
                backend=default_backend(),  # noqa: E501
            )
            decryptor = cipher.decryptor()
            data = decryptor.update(ciphertext) + decryptor.finalize()
        except Exception as e:
            raise ValueError("Invalid ciphertext, unable to decrypt.") from e

        return self._unpad(data)

    def export_key(self):
        return self.key

    def import_key(self, key):
        self.key = key

    def set_mode(self, mode):
        self.mode = mode

    def _pad(self, data):
        padder = padding.PKCS7(128).padder()
        padded_data = padder.update(data) + padder.finalize()
        return padded_data

    def _unpad(self, padded_data):
        """
        Removes padding from the input data.
        """
        try:
            unpadder = padding.PKCS7(128).unpadder()
            data = unpadder.update(padded_data) + unpadder.finalize()
        except Exception as e:
            raise ValueError("Invalid ciphertext, unable to decrypt.") from e

        return data

    def wrap_key(self, key):
        """
        Wraps the input key using the AES Key Wrapper (AESKW) algorithm with
        the current key.
        """
        return keywrap.aes_key_wrap(self.key, key, default_backend())

    def unwrap_key(self, wrapped_key):
        """
        Unwraps the input wrapped key using the AES Key Wrapper (AESKW) algo
        with the current key.
        """
        return keywrap.aes_key_unwrap(self.key, wrapped_key, default_backend())
