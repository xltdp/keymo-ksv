from typing import Union
from cryptography.hazmat.primitives.kdf.scrypt import Scrypt
from cryptography.hazmat.backends import default_backend


class SCRYPT:
    """
    A class that implements the Scrypt Key Derivation Function (KDF) to encrypt
    and decrypt data using a password and salt.

    :param password: The password used to encrypt and decrypt data
    :type password: Union[bytes, str]
    :param salt: A salt value used to add randomness to the encryption process
    :type salt: Union[bytes, str]
    :param N: CPU/memory cost parameter (default=16384)
    :type N: int
    :param r: Block size parameter (default=8)
    :type r: int
    :param p: Parallelization parameter (default=1)
    :type p: int
    """

    def __init__(
        self,
        password: Union[bytes, str],
        salt: Union[bytes, str],
        N: int = 16384,
        r: int = 8,
        p: int = 1,
    ):
        # Ensure the password is encoded in bytes
        self.password = (
            password.encode() if isinstance(password, str) else password
        )  # noqa: E501
        # Ensure the salt is encoded in bytes
        self.salt = salt.encode() if isinstance(salt, str) else salt
        self.N = N
        self.r = r
        self.p = p
        self.backend = default_backend()

    def _create_kdf(self) -> Scrypt:
        """
        Creates a new instance of the SCRYPT key derivation function.
        :return: Scrypt instance
        :rtype: Scrypt
        """
        return Scrypt(
            salt=self.salt,
            length=64,
            n=self.N,
            r=self.r,
            p=self.p,
            backend=self.backend,
        )

    def encrypt(self, message: str) -> bytes:
        """
        Encrypts the provided message.
        :param message: message to encrypt
        :type message: str
        :return: encrypted message
        :rtype: bytes
        """
        kdf = self._create_kdf()
        key = kdf.derive(self.password)
        return message.encode() + key

    def decrypt(self, encrypted_message: bytes) -> str:
        """
        Decrypts the provided encrypted message.
        :param encrypted_message: encrypted message to decrypt
        :type encrypted_message: bytes
        :return: decrypted message
        :rtype: str
        """
        kdf = self._create_kdf()
        key = kdf.derive(self.password)  # noqa: F841
        return encrypted_message[:-64].decode()

    def generate_key(self) -> bytes:
        """
        Generates a key from the password and salt.
        :return: key
        :rtype: bytes
        """
        kdf = self._create_kdf()
        return kdf.derive(self.password)

    def verify(self, encrypted_message: bytes) -> bool:
        """
        Verifies that the provided encrypted message was encrypted using
        the same password and salt.
        :param encrypted_message: encrypted message to verify
        :type encrypted_message: bytes
        :return: True if encrypted_message was encrypted using the same
        password and salt, False otherwise.
        :rtype: bool
        """
        kdf = self._create_kdf()
        key = kdf.derive(self.password)
        return encrypted_message.endswith(key)

    def wrap(self, data: bytes) -> bytes:
        """
        Wrap the given key and data using a derived key.

        :param key: The key to be wrapped.
        :param data: The data to be wrapped.
        :return: The wrapped key and data.
        """
        # Create a KDF object
        kdf = self._create_kdf()
        # Derive the key from the password
        derived_key = kdf.derive(self.password)
        # Concatenate the derived key and data
        return derived_key + data

    def unwrap(self, wrapped_key_and_data: bytes) -> bytes:
        """
        Unwrap the given wrapped key and data using a derived key.

        :param wrapped_key_and_data: The wrapped key and data.
        :return: The original key and data.
        :raise: ValueError if the password is incorrect.
        """
        # Create a KDF object
        kdf = self._create_kdf()
        # Derive the key from the password
        derived_key = kdf.derive(self.password)
        # Check if the wrapped key and data starts with the derived key
        if not wrapped_key_and_data.startswith(derived_key):
            # Raise an error if the password is incorrect
            raise ValueError("Invalid password")
        # Return the original data by removing the derived key
        return wrapped_key_and_data[len(derived_key):]  # fmt: skip
