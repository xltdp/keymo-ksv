import os
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms
from cryptography.hazmat.primitives.poly1305 import Poly1305

# Poly1305 is a fast, secure and widely-used authenticated encryption algorithm
# It's typically used in combination with a symmetric cipher such as ChaCha20
# for message authentication.

# ChaCha20 encrypts the message and Poly1305 generates a message authentication
# code (MAC) from the encrypted message, which can be used to detect tampering
# with the message. The receiver can then use the same key with Poly1305 to
# verify the MAC, ensuring that the message has not been tampered with and the
# sender is authentic.

# This combination provides:
#   confidentiality ( via ChaCha20) and authenticity ( via Poly1305).


class ChaCha20Poly1305:
    def __init__(self, password, salt):
        self.backend = default_backend()
        self.salt = salt
        self.key = self._derive_key(password)
        self.nonce = os.urandom(16)

    def _derive_key(self, password):
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=self.salt,
            iterations=100000,
            backend=self.backend,
        )
        return kdf.derive(password)

    def encrypt(self, data):
        cipher = Cipher(
            algorithms.ChaCha20(self.key, self.nonce),
            None,
            backend=self.backend,  # noqa: E501
        )
        encryptor = cipher.encryptor()
        encrypted_data = encryptor.update(data) + encryptor.finalize()

        poly = Poly1305(self.key)
        poly.update(encrypted_data)
        self.tag = poly.finalize()

        return encrypted_data, self.tag

    def decrypt(self, encrypted_data, tag):
        poly = Poly1305(self.key)
        poly.update(encrypted_data)
        try:
            poly.verify(tag)
        except Exception:
            raise ValueError(
                "Data has been tampered with or password is incorrect"
            )  # noqa: E501

        cipher = Cipher(
            algorithms.ChaCha20(self.key, self.nonce),
            None,
            backend=self.backend,  # noqa: E501
        )
        decryptor = cipher.decryptor()
        return decryptor.update(encrypted_data) + decryptor.finalize()

    def verify(self, encrypted_data, tag):
        poly = Poly1305(self.key)
        poly.update(encrypted_data)
        try:
            poly.verify(tag)
        except Exception:
            return False
        return True

    def get_tag(self):
        return self.tag
