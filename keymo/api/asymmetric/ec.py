from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric import ec


class EC:
    def __init__(self):
        self.private_key = None
        self.public_key = None

    def generate_keys(self, curve="secp256k1"):
        curves = {
            "secp256k1": ec.SECP256K1,
            "secp384r1": ec.SECP384R1,
            "secp521r1": ec.SECP521R1,
            "prime256v1": ec.PRIME256V1,
        }

        if curve not in curves:
            raise ValueError("Invalid curve name")

        private_key = ec.generate_private_key(curves[curve], default_backend())
        public_key = private_key.public_key()

        self.private_key = private_key
        self.public_key = public_key

    def encrypt(self, data, public_key):
        raise NotImplementedError("ECC encryption is not supported")

    def decrypt(self, ciphertext):
        raise NotImplementedError("ECC decryption is not supported")

    def export_private_key(self, password):
        if self.private_key:
            pem = self.private_key.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.PKCS8,
                encryption_algorithm=serialization.BestAvailableEncryption(
                    password
                ),  # noqa: E501
            )
            return pem

    def export_public_key(self):
        if self.public_key:
            pem = self.public_key.public_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PublicFormat.SubjectPublicKeyInfo,
            )
            return pem

    def import_private_key(self, pem, password):
        private_key = serialization.load_pem_private_key(
            pem, password=password, backend=default_backend()
        )
        self.private_key = private_key

    def import_public_key(self, pem):
        public_key = serialization.load_pem_public_key(
            pem, backend=default_backend()
        )  # noqa: E501
        self.public_key = public_key

    def sign(self, data):
        signature = self.private_key.sign(data, ec.ECDSA(hashes.SHA256()))
        return signature

    def verify(self, signature, data):
        try:
            self.public_key.verify(signature, data, ec.ECDSA(hashes.SHA256()))
            return True
        except Exception:
            return False
