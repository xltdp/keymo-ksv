# AES
This class contains the following methods:
- `__init__(self, key=None, mode=modes.CBC)`: Initializes the class with an optional key and mode of operation. If no key is provided, a random one is generated. If no mode is provided, CBC is used by default.
- `generate_key(self, key_size=256)`: Generates a random key of the specified size, in bytes.
- `encrypt(self, data)`: Encrypts the input data using the selected mode of operation and key.
- `decrypt(self, ciphertext)`: Decrypts the input ciphertext using the selected mode of operation and key.
- `wrap_key(self, key)`: Wraps the input key using the AES Key Wrapper (AESKW) algorithm with the current key.
- `unwrap_key(self, wrapped_key)`: Unwraps the input wrapped key using the AES Key Wrapper (AESKW) algorithm with the current key.
- `_pad(self, data)`: Adds padding to the input data to align it with the block size.
- `_unpad(self, data)`: Removes padding from the input data.
