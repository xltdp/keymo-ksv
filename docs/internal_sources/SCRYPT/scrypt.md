## Class SCRYPT:
A class that implements the Scrypt Key Derivation Function (KDF) to encrypt and decrypt data using a password and salt.

### Parameters
- password: The password used to encrypt and decrypt data (bytes or str)
- salt: The salt used to add randomness to the encryption process (bytes or str)
- N: CPU/memory cost parameter (default=16384) (int)
- r: Block size parameter (default=8) (int)
- p: Parallelization parameter (default=1) (int)

### Methods
- encrypt(message: str) -> bytes:
  Encrypts the given message.
  - message: message to encrypt
  - return: encrypted message as bytes

- decrypt(encrypted_message: bytes) -> str:
  Decrypts the given encrypted message.
  - encrypted_message: encrypted message to decrypt
  - return: decrypted message as str

- generate_key() -> bytes:
  Generates a key from the password and salt.
  - return: key as bytes

- verify(encrypted_message: bytes) -> bool:
  Verifies if the encrypted message was encrypted using the same password and salt.
  - encrypted_message: encrypted message to verify
  - return: True if encrypted using the same password and salt, False otherwise.

- wrap(data: bytes) -> bytes:
  Wraps the given data using a derived key.
  - data: The data to be wrapped.
  - return: The wrapped key and data.

- unwrap(wrapped_key_and_data: bytes) -> bytes:
  Unwraps the given wrapped data using a derived key.
  - wrapped_key_and_data: The wrapped key and data.
  - return: The original data.
  - raise: ValueError if the password is incorrect.
